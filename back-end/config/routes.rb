Rails.application.routes.draw do
  root "pages#show", page: "root"
  resources :sessions
  resources :users
  resources :comments
  resources :campaigns do
    resources :todos, only: [:new, :create]
  end
  resources :todos, only: [:destroy]
end
