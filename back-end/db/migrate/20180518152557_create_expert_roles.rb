class CreateExpertRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :expert_roles do |t|
      t.string :profession
      t.string :service

      t.timestamps
    end
  end
end
