# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create user expert
expert = User.create(email: "test@test.com", password: "password")
expert.role = ExpertRole.create(profession: "lawyer")
expert.save

users = User.create([{ email: 'superman@gmail.com', password: "password" },
             { email: 'batman@gmail.com', password: "password" },
             { email: 'spiderman@gmail.com', password: "password" }])


# Create campaigns
expert_campaigns = Campaign.
  create([{ title: 'Expert Campaigns 1', tags: 'expert tag', estimated_duration: '2 hours'},
       { title: 'Expert Campaigns 2', tags: 'expert tag', estimated_duration: '2 days'}])


novice_campaigns = Campaign.
  create([{ title: 'Novice Campaign 1', tags: 'tags novice', estimated_duration: '5 hours'},
           { title: 'Novice Campaign 2', tags: 'tag novice', estimated_duration: '1 month'}])

expert_campaigns.each do |compaign|
  compaign.user = expert
  compaign.save
end

novice_campaigns.each do |compaign|
  compaign.user = users[0]
  compaign.save
end


# Create TODOS
todos = %w(todo1 todo2 todo3 todo4 todo5)

expert_campaigns.each do |compaign|
  todos.each do |title|
    Todo.create(title: "expert #{title}", campaign_id: compaign.id) #
  end
end

novice_campaigns.each do |compaign|
  todos.each do |title|
    Todo.create(title: "novice #{title}", campaign_id: compaign.id) #
  end
end
