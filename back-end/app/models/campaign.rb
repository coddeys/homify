class Campaign < ApplicationRecord
  belongs_to :user
  has_many :todos
  has_many :comments, as: :commentable
  validates :title, presence: true
end
