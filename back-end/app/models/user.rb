class User < ApplicationRecord
  has_secure_password

  enum status: %w(qualified not_qualified banned)

  validates :email, presence: true
  validates :email, uniqueness: true

  has_many :campaigns
  belongs_to :role, polymorphic: true, optional: true

  def novice?
    role.nil?
  end

  def expert?
    !novice?
  end

  def type
    return "Novice" if novice?
    "Expert"
  end
end
