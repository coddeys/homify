class ExpertRole < ApplicationRecord
  has_one :user, as: :role
end
