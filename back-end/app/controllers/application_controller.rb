class ApplicationController < ActionController::Base
  helper_method :current_user, :authorize, :expert

  private

  def current_user
    @current_user = User.find(session[:user_id]) if session[:user_id]
  end

  def authorize
    redirect_to new_session_url, alert: "Not authorized" if current_user.nil?
  end

  def expert
    redirect_to new_session_url, alert: "Not expert" if current_user.nil? || current_user.novice?
  end
end
