class TodosController < ApplicationController
  before_action :expert, only: [:edit, :update, :destroy]
  before_action :set_todo, only: [:show, :edit, :update, :destroy]

  def new
    @campaign = Campaign.find params[:campaign_id]
    @todo = Todo.new(campaign: @campaign)
  end

  def create
    @todo = Todo.new(todo_params)

    respond_to do |format|
      if @todo.save
        format.html { redirect_to campaign_path(@todo.campaign), notice: 'Todo was successfully created.' }
        format.json { render :show, status: :created, location: @todo }
      else
        format.html { render :new, @campaign }
        format.json { render json: @todo.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @todo.destroy
    respond_to do |format|
      format.html { redirect_to campaign_url(@todo.campaign), notice: 'Todo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_todo
      @todo = Todo.find(params[:id])
    end

    def todo_params
      params.require(:todo).permit(:title, :campaign_id)
    end
end
