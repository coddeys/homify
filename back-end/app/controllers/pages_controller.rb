class PagesController < ApplicationController
  def show
    render template: "pages/root"
  end
end
