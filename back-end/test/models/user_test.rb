require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'invalid without email' do
    user = User.new(username: "username")
    assert_not user.valid?
  end

  test "by default user should be novice" do
    user = users(:one)
    assert user.novice?
  end

  test "should be expert" do
    user = users(:one)
    user.role = ExpertRole.create
    assert user.expert?
  end

  test "novice role type return Novice" do
    user = users(:one)
    assert_equal "Novice", user.type
  end

  test "expert role type return Expert" do
    user = users(:one)
    user.role = ExpertRole.create
    assert_equal "Expert", user.type
  end
end
