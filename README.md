# Homify Ruby Backend Developer Task

## Develop

In back-end directory you can find ruby on rails app.

```shell
rake db:create
rake db:migrate
rake db:seed
rails s
```

It's not possible to create Expert user in web UI.
The way around it
1. Sign up
2. Log out
3. Login with expert user from seed
email: test@test.com
password: password
4. Change your user type to ExpertRole

And of coruse it's possible to do that in rails console

## Test

Run test
`rake test`

## Deploy

Build docker images
```shell
docker-compose down
docker-compose build
```

If it's a first time seed db
```shell
docker-compose run back-end rake db:create
docker-compose run back-end rake db:migrate
docker-compose run back-end rake db:seed
```

Run docker impages
`docker-compose up`

Then open in a browser `http://localhost:3000/`

Ideally speaking it should work on localhost as well, but something wrong
with nginx.conf file, so rails can't verify CSRF token authenticity.
So sign up and login do not work on `http::/localhost`

## TODO
Here is a short list with most important things that I would like to improve if you have more time.

1. Finish UI for comments
2. Use `gem pudnit` for authorization
3. Write more tests especcially for a business logic
4. Write better UI with modern frameworks like Elm or React
5. Modify nginx so make CSRF workable in docker
6. Make deploy script for QA person
